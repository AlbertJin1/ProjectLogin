import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyAMiZBRqwrKm-vmTnjJc2BicDPi5yj9n3w",
    authDomain: "projectlogin-da233.firebaseapp.com",
    projectId: "projectlogin-da233",
    storageBucket: "projectlogin-da233.appspot.com",
    messagingSenderId: "737455882588",
    appId: "1:737455882588:web:36f6ee22e05d0fccd3c199"
}

if (!firebase.app.length) {
    firebase.initializeApp(firebaseConfig);
}

export { firebase };