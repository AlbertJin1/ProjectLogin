import React, { useContext } from 'react';
import { View, Text, Button } from 'react-native';
import { SessionContext, LOGOUT } from '../components(DRAFT)/SessionContext';
import UserProfileStyles from '../styles/UserProfileStyles';

const UserProfileScreen = ({ navigation }) => {
  const { sessionState, dispatch } = useContext(SessionContext);

  const handleLogout = () => {
    dispatch({ type: LOGOUT });
    navigation.navigate('Login');
  };

  const username = sessionState.user ? sessionState.user.username : 'Guest';

  return (
    <View style={UserProfileStyles.container}>
      <Text style={UserProfileStyles.welcomeText}>
        Welcome, {username}
      </Text>
      <Button
        title="Logout"
        onPress={handleLogout}
        style={UserProfileStyles.logoutButton}
      />
    </View>
  );
};

export default UserProfileScreen;