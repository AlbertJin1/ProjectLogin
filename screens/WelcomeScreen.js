import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import WelcomeScreenStyles from '../styles/WelcomeStyles';

const WelcomeScreen = () => {
  const navigation = useNavigation();

  const handleGetStarted = () => {
    navigation.navigate('Login');
  };

  return (
    <View style={WelcomeScreenStyles.container}>
      <Text style={WelcomeScreenStyles.title}>Welcome AmbatuProfs</Text>
      <TouchableOpacity
        style={WelcomeScreenStyles.getStartedButton}
        onPress={handleGetStarted}
      >
        <Text style={WelcomeScreenStyles.buttonText}>GET STARTED</Text>
      </TouchableOpacity>
    </View>
  );
};

export default WelcomeScreen;